import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {

  public contacts: Contact[];
  ModalTitle: string;
  ActivateAddEditContactComp: boolean = false;

  ngOnInit(): void {
    this.refreshContactList();
  }

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) { }


  //addClick() {
  //  this.contact = {
  //    DepartmentId: 0,
  //    DepartmentName: ""
  //  }
  //  this.ModalTitle = "Add Department";
  //  this.ActivateAddEditContactComp = true;

  //}

  //editClick(item) {
  //  this.contact = item;
  //  this.ModalTitle = "Edit Department";
  //  this.ActivateAddEditContactComp = true;
  //}

  deleteClick(item) {
    if (confirm('Are you sure?')) {
      this.http.delete<Contact[]>(this.baseUrl + 'contacts/deletecontact?Id=' + item).subscribe(result => {
        this.refreshContactList();
      }, error => console.error(error));
    }
  }

  closeClick() {
    this.ActivateAddEditContactComp = false;
    this.refreshContactList();
  }

  refreshContactList() {
    this.http.get<Contact[]>(this.baseUrl + 'contacts').subscribe(result => {
      this.contacts = result;
    }, error => console.error(error));
  }

}

interface Contact {
  id: Int16Array;
  name: string;
  address: string;
  phoneNumber: string;
}

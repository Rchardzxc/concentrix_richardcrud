﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Contants;
using WebAPI.Data;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ContactsController : ControllerBase
    {
        private readonly DataContext _dataContext;
        private readonly ILogger<ContactsController> _logger;

        public ContactsController(DataContext dataContext, ILogger<ContactsController> logger)
        {
            _dataContext = dataContext;
            _logger = logger;
        }

        [HttpGet]
        public IActionResult GetContacts()
        {
            return Ok(_dataContext.Contacts.ToList());
        }

        [HttpPost("AddContact")]
        public async Task<IActionResult> AddContact([FromBody] ContactContract contactContract)
        {
            var newContact = new Contact
            {
                Name = contactContract.Name,
                Address = contactContract.Address,
                PhoneNumber = contactContract.PhoneNumber
            };

            await _dataContext.Contacts.AddAsync(newContact);
            await _dataContext.SaveChangesAsync();

            return Ok();
        }

        [HttpPost("EditContact")]
        public async Task<IActionResult> EditContact([FromBody] ContactContract contactContract)
        {
            var contact = _dataContext.Contacts.Where(x => x.Id == contactContract.Id).FirstOrDefault();

            contact.Name = contactContract.Name;
            contact.Address = contactContract.Address;
            contact.PhoneNumber = contactContract.PhoneNumber;

            _dataContext.Contacts.Update(contact);
            await _dataContext.SaveChangesAsync();

            return Ok();
        }

        [HttpDelete("DeleteContact")]
        public async Task<ActionResult<bool>> DeleteContact(int Id)
        {
            var contact = _dataContext.Contacts.Where(x => x.Id == Id).FirstOrDefault();

            _dataContext.Contacts.Remove(contact);
            await _dataContext.SaveChangesAsync();

            return Ok();
        }
    }
}

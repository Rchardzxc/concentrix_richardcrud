import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {DepartmentComponent} from './contact/department.component';


const routes: Routes = [
{path:'contact',component:DepartmentComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

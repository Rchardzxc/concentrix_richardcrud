import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})

export class SharedService {
readonly APIUrl="https://localhost:44335";
readonly PhotoUrl = "http://localhost:44335/Photos/";


  constructor(private http:HttpClient) { }

  getContactList():Observable<any[]>{
    return this.http.get<any>(this.APIUrl+'/Contacts');
  }

  addContact(val:any){
    return this.http.post(this.APIUrl+'/Contacts/AddContact',val);
  }

  updateContact(val:any){
    return this.http.post(this.APIUrl+'/Contacts/EditContact',val);
  }

  deleteContact(val:any){
    return this.http.delete(this.APIUrl+'/Contacts/DeleteContact?Id='+val);
  }

  getEmpList():Observable<any[]>{
    return this.http.get<any>(this.APIUrl+'/Employee');
  }

  addEmployee(val:any){
    return this.http.post(this.APIUrl+'/Employee',val);
  }

  updateEmployee(val:any){
    return this.http.put(this.APIUrl+'/Employee',val);
  }

  deleteEmployee(val:any){
    return this.http.delete(this.APIUrl+'/Employee/'+val);
  }


  UploadPhoto(val:any){
    return this.http.post(this.APIUrl+'/Employee/SaveFile',val);
  }

  getAllDepartmentNames():Observable<any[]>{
    return this.http.get<any[]>(this.APIUrl+'/Employee/GetAllDepartmentNames');
  }
}
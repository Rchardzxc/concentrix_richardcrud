import { Component, OnInit,Input } from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-add-edit-dep',
  templateUrl: './add-edit-dep.component.html',
  styleUrls: ['./add-edit-dep.component.css']
})
export class AddEditDepComponent implements OnInit {

  constructor(private service:SharedService) { }

  @Input() dep:any;
  id!: Int16Array;
  name!:string;
  address!: string;
  phoneNumber!: string;

  ngOnInit(): void {
    this.id=this.dep.id;
    this.name=this.dep.name;
    this.address=this.dep.address;
    this.phoneNumber=this.dep.phoneNumber;
  }

  addContact(){
    var val = {name:this.name,address:this.address,phoneNumber:this.phoneNumber};
    this.service.addContact(val).subscribe(res=>{
      alert('Added successfully');
    });
  }

  updateContact(){
    var val = {id:this.id,name:this.name,address:this.address,phoneNumber:this.phoneNumber};
    this.service.updateContact(val).subscribe(res=>{
      alert('Updated successfully');
    });
  }

}

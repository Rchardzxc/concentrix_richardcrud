import { Component, OnInit } from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-show-dep',
  templateUrl: './show-dep.component.html',
  styleUrls: ['./show-dep.component.css']
})
export class ShowDepComponent implements OnInit {

  constructor(private service:SharedService) { }

  ContactList:any=[];

  ModalTitle!: string;
  ActivateAddEditDepComp:boolean=false;
  dep:any;


  ngOnInit(): void {
    this.refreshDepList();
  }

  addClick(){
    this.dep={
      id:0,
      name:"",
      address:"",
      phoneNumber:"",
    }
    this.ModalTitle="Add Contact";
    this.ActivateAddEditDepComp=true;

  }

  editClick(item: any){
    this.dep=item;
    this.ModalTitle="Edit Contact";
    this.ActivateAddEditDepComp=true;
  }

  deleteClick(item: any){
    if(confirm('Are you sure?')){
      this.service.deleteContact(item.id).subscribe(data=>{
        alert('Deleted successfully');
        this.refreshDepList();
      })
    }
  }

  closeClick(){
    this.ActivateAddEditDepComp=false;
    this.refreshDepList();
  }


  refreshDepList(){
    this.service.getContactList().subscribe(data=>{
      this.ContactList=data;
    });
  }

}
